### Repo structure


#### Core
Contains Input/output files for the dataset

* `base_io` - basic input output functions
* `analysis_io` - extended class for the base io, has utilities
for analysis
* `/files_from_neo_09dev` - some essential files that area loaded 
by the `base_io` class


#### preprocessing
Contains the scripts to prepare the dataset for analysis.

`preprocessing_pipeline` contains the code to run all the 
scripts in this path.