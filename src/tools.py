import neo
import numpy as np
import plotly.graph_objects as go
import plotly
import pickle
from pathlib import Path
from scipy.signal import find_peaks
import math
from scipy.interpolate import interp1d
from src import config as cfg
from src import Io
from plotly.io import write_image
from plotly.subplots import make_subplots
import pandas as pd
import quantities as qt
import multiprocessing as mp


def detect_peak(data, min_idx=0):
    assert isinstance(data, np.ndarray), 'data should be a numpy array'
    if len(data.shape) == 1:
        p, _ = find_peaks(data)
        p = p[p >= min_idx][0]
        t, _ = find_peaks(-data)
        t = t[t >= min_idx][0]
        return t, p
    else:
        n = data.shape[0]
        all_p = np.zeros((n))
        all_t = []
        for i in range(n):
            p, _ = find_peaks(data[i, :])
            if p.shape[0] == 0:
                all_p.append(None)
            else:
                idx = p >= min_idx
                if np.any(idx):
                    p = p[idx][0]
                else:
                    p = None
                all_p.append(p)

            t, _ = find_peaks(-data[i, :])
            if t.shape[0] == 0:
                all_t.append(None)
            else:
                idx = t >= min_idx
                if np.any(idx):
                    t = t[idx][0]
                else:
                    t = None
                all_t.append(t)

        assert len(all_t) == data.shape[0]
        assert len(all_p) == data.shape[0]
        return all_t, all_p


def _get_filename(filename, big_file=False):
    if not big_file:
        return cfg.dump_dir / f'{filename}.pickle'
    elif big_file:
        return Path('D:\dmp_tmp') / f'{filename}.pickle'


def is_saved(savename: str, on_cluster=False, big_file=False):
    savename = _get_filename(savename, big_file)
    if savename.is_file():
        return True
    else:
        return False


def save_data(data: dict, savename: str, big_file=False):
    savename = _get_filename(savename, big_file)

    if not isinstance(data, dict):
        raise ValueError(f'data should be a dict')

    if not savename.parent.is_dir():
        savename.parent.mkdir(parents=True)

    with open(savename.as_posix(), 'wb') as f:
        pickle.dump(data, f, protocol=4)


def load_data(loadname: str, big_file=False):
    loadname = _get_filename(loadname, big_file)

    if not loadname.is_file():
        raise ValueError(f'{loadname} does not exist')

    with open(loadname.as_posix(), 'rb') as f:
        data = pickle.load(f)

    return data


def save_fig(fig: go.Figure, savename: str, formats=None, scale=1, verbose=True):
    savename = cfg.result_dir / savename
    if formats is None:
        formats = ['png']

    if not savename.parent.is_dir():
        savename.parent.mkdir(parents=True)

    for f in formats:
        file = savename.parent / (savename.name + f'.{f}')
        write_image(
            fig=fig,
            file=file,
            format=f,
            scale=1 if f != 'png' else 10,  # not used when exporting to svg
            engine='kaleido',
            # width=300,
            # height=180,
        )

        if verbose:
            print(f'saved: {file}')


def hex_to_rgba(hx, opacity, as_array=False):
    h = hx.lstrip('#')
    res = f'{tuple(int(h[i:i + 2], 16) for i in (0, 2, 4))}'

    if not as_array:
        return f'rgba{res[:-1]}, {opacity})'
    elif as_array:
        res = res.split('(')[1].split(')')[0].split(',')
        res.append(opacity)
        return [int(r) for r in res]


def np_from_qt(qt_input, rescale_units):
    return qt_input.rescale(rescale_units).magnitude.flatten()


def find_times_idx(times, t0, t1):
    t0 = t0.rescale('s').magnitude
    t1 = t1.rescale('s').magnitude
    times = times.rescale('s').magnitude
    idx = np.where((times >= t0) & (times <= t1))[0]
    return idx


def plot_sigbar(f: go.Figure, x0name: str, x1name: str, y: [float, str],
                x0val: float, x1val: float, r=None, c=None, linewidth=None,
                showstar=True):
    """
    Plots a significance bar on top of a figure
    Parameters
    ----------
    c
        column in subfigure to plot in
    r
        row of subplot in figure to plot in
    y
        heigth of errorbar
    f
        figure to plot data in
    x1val
        numeric value of x rightside errorbar
    x0val
        numeric value of x leftside errorbar
    x1name
        name of category rigtside errorbar
    x0name
        name of category leftside errobar


    Returns
    -------
        Figure containing an errorbar!
    """
    if r is not None:
        f.add_trace(go.Scatter(
            x=[x0name, x1name],
            y=[y, y],
            mode='lines',
            line=dict(color='black', width=1 if linewidth is None else linewidth),
            showlegend=False,
        ), row=r, col=c)

        if showstar:
            f.add_annotation(
                x=(x1val + x0val) / 2,
                y=y + y * 0.01,
                text='*',
                showarrow=False,
                row=r, col=c
            )
    else:
        f.add_trace(go.Scatter(
            x=[x0name, x1name],
            y=[y, y],
            mode='lines',
            line=dict(color='black', width=1 if linewidth is None else linewidth),
            showlegend=False,
        ))

        if showstar:
            f.add_annotation(
                x=(x1val + x0val) / 2,
                y=y + y * 0.01,
                text='*',
                showarrow=False,
            )

    return f


class PBar:
    def __init__(self, text, n_ticks, n_counts, n_decimals=0):
        self.text = text
        self.n_ticks = n_ticks
        self.n_counts = n_counts
        self.n_chars = len(text) + n_ticks
        self.count = 0
        self.n_decimals = n_decimals

    def print(self, count):
        self.count = count
        if count == 0:
            perc = 0
        else:
            perc = self.count / self.n_counts
        tick = math.ceil(perc * self.n_ticks)
        pstr = '\r' + self.text + ': |'

        for _ in range(tick):
            pstr += '+'
        for _ in range(self.n_ticks - tick):
            pstr += '_'

        perc_print = f'{perc * 100:.20f}'
        pstr += '|' + f' {perc_print[:self.n_decimals + 1]} %'
        print(pstr, end='')

    def print_1(self):
        self.print(self.count + 1)

    def close(self):
        self.print(count=self.n_counts)
        print('')


def interp_color(nsteps, step_nr, scalename, alpha, inverted=False):
    cols = getattr(getattr(plotly.colors, scalename[0]), scalename[1])
    if inverted:
        cols = cols[::-1]
    xx = np.linspace(0, 1, len(cols))

    if 'rgb' in cols[0]:
        r = [int(t.split('(')[1].split(',')[0]) for t in cols]
        g = [int(t.split('(')[1].split(',')[1]) for t in cols]
        b = [int(t.split('(')[1].split(',')[2].split(')')[0]) for t in cols]
    else:
        r = [int(tuple(int(t.lstrip('#')[i:i + 2], 16) for i in (0, 2, 4))[0]) for t in cols]
        g = [int(tuple(int(t.lstrip('#')[i:i + 2], 16) for i in (0, 2, 4))[1]) for t in cols]
        b = [int(tuple(int(t.lstrip('#')[i:i + 2], 16) for i in (0, 2, 4))[2]) for t in cols]

    fr = interp1d(xx, r)
    fg = interp1d(xx, g)
    fb = interp1d(xx, b)

    ii = step_nr / nsteps
    return f'rgba({fr(ii)}, {fg(ii)}, {fb(ii)}, {alpha})'


def get_session_id_from_lfp_id(lfp_id: str) -> str:
    parts_1 = lfp_id.split('_')
    parts_2 = parts_1[1].split('-')
    return f'rat-{parts_2[1]}_{parts_2[2]}'


def get_session_id_from_unit_id(unit_id: str) -> str:
    parts = unit_id.split('_')
    date = parts[1][:-4]

    sid = None
    for s in prm.session_ids:
        if date in s:
            sid = s

    assert sid is not None
    return sid


def make_figure(nrows, ncols, width, height=0, **kwargs) -> go.Figure:
    for i, k in kwargs.items():
        assert i in ['x_spacing', 'y_spacing', 'margin_l', 'margin_r', 'margin_b', 'margin_t',
                     'shared_xaxes', 'shared_yaxes', 'axheights', 'axwidths', 'subplot_titles',
                     'specs', 'equal_width_height'], f'{i}'

    font_famliy = 'calibri'
    figwidth_pxl = 595 * width
    figheight_pxl = 842 * 0.25 * height
    if 'equal_width_height' in kwargs.keys():
        if isinstance(kwargs['equal_width_height'], bool) and kwargs['equal_width_height']:
            figheight_pxl = figwidth_pxl


    min_margin_l_pxl = 0
    min_margin_r_pxl = 0
    min_margin_b_pxl = 0
    min_margin_t_pxl = 20

    min_margin_l = min_margin_l_pxl / figwidth_pxl
    min_margin_r = min_margin_r_pxl / figwidth_pxl
    min_margin_b = min_margin_b_pxl / figheight_pxl
    min_margin_t = 0

    if 'margin_l' not in kwargs.keys() or kwargs['margin_l'] < min_margin_l:
        margin_l = min_margin_l
    else:
        margin_l = kwargs['margin_l']
    if 'margin_r' not in kwargs.keys() or kwargs['margin_r'] < min_margin_r:
        margin_r = min_margin_r
    else:
        margin_r = kwargs['margin_r']
    if 'margin_b' not in kwargs.keys() or kwargs['margin_b'] < min_margin_b:
        margin_b = min_margin_b
    else:
        margin_b = kwargs['margin_b']
    if 'margin_t' not in kwargs.keys() or kwargs['margin_t'] < min_margin_t:
        margin_t = min_margin_t
    else:
        margin_t = kwargs['margin_t']

    if 'x_spacing' in kwargs.keys():
        x_spacing = kwargs['x_spacing']
    elif 'axwidths' in kwargs.keys():
        x_spacing = (1 - margin_r - margin_l - np.sum(kwargs['axwidths'])) / (ncols - 1)
    else:
        x_spacing = 0.1

    if 'y_spacing' in kwargs.keys():
        y_spacing = kwargs['y_spacing']
    elif 'axheights' in kwargs.keys():
        y_spacing = (1 - margin_t - margin_b - np.sum(kwargs['axheights'])) / (nrows -1)
    else:
        y_spacing = 0.1

    if 'axheights' in kwargs.keys():
        axheights = kwargs['axheights']
        if isinstance(y_spacing, list):
            ys = np.sum(y_spacing)
        else:
            ys = (nrows-1)*y_spacing
        assert np.sum(axheights) + margin_t + margin_b + ys <= 1
    else:
        axheights = [(1 - margin_t - margin_b - (nrows-1)*y_spacing) / nrows for _ in range(nrows)]

    if 'axwidths' in kwargs.keys():
        axwidths = kwargs['axwidths']
    else:
        axwidths = [(1 - margin_l - margin_r - (ncols - 1) * x_spacing) / ncols for _ in range(ncols)]

    specs_f = dict()
    if 'shared_xaxes' in kwargs.keys():
        specs_f['shared_xaxes'] = kwargs['shared_xaxes']
    # if 'subplot_titles' in kwargs.keys():
    #     specs_f['subplot_titles'] = kwargs['subplot_titles']
    if 'specs' in kwargs.keys():
        for i, k in kwargs.items():
            specs_f[i] = k

    fig = make_subplots(rows=nrows, cols=ncols, **specs_f)

    fig.update_layout(
        # Figure dimensions
        autosize=False,
        width=figwidth_pxl,
        height=figheight_pxl,
        margin=dict(l=0, t=0, b=0, r=0),
        plot_bgcolor='white',
        paper_bgcolor='white',
        legend=dict(
            borderwidth=0.5,
            bordercolor='black',
            font=dict(
                family=font_famliy,
                size=6,
            )
        ),
    )

    ax_map = dict()
    ax_tick = 1
    for row_i in range(nrows):
        for col_i in range(ncols):
            if ax_tick == 1:
                suffix = ''
            else:
                suffix = f'{ax_tick}'

            x_offset = 0
            for i in range(col_i):
                if isinstance(x_spacing, list):
                    if col_i == 0:
                        xs = 0
                    else:
                        xs = x_spacing[i-1]
                else:
                    xs = x_spacing

                x_offset += axwidths[i] + xs

            x0 = margin_l + x_offset
            x1 = x0 + axwidths[col_i]

            y_offset = 0
            for i in range(row_i):
                if isinstance(y_spacing, list):
                    if row_i == 0:
                        ys = 0
                    else:
                        ys = y_spacing[i-1]
                else:
                    ys = y_spacing
                y_offset += axheights[i] + ys

            y1 = 1 - margin_t - y_offset
            y0 = y1 - axheights[row_i]

            if 'subplot_titles' in kwargs.keys():
                # print(kwargs['subplot_titles'], ax_tick-1)
                fig.add_annotation(
                    x=x0, y=y1+0.02,
                    text=kwargs['subplot_titles'][ax_tick-1],
                    font=dict(size=8, family=font_famliy),
                    showarrow=False,
                    xanchor='left', yanchor='bottom',
                    xref='paper', yref='paper',
                )


            ax_tick += 1

            ax_map[(row_i, col_i)] = dict(
                xname='xaxis' + suffix,
                xaxis='x' + suffix,
                row=row_i+1,
                yname='yaxis' + suffix,
                yaxis='y' + suffix,
                col=col_i+1,
                domain_x=[x0, x1],
                domain_y=[y0, y1],
            )


            fig.update_xaxes(
                row=ax_map[(row_i, col_i)]['row'],
                col=ax_map[(row_i, col_i)]['col'],
                # automargin=False,
                domain=[x0, x1],

                # Xaxis ticks
                tickmode='array',
                tickvals=[],
                tickwidth=0.5,
                ticklen=0.1,
                tickangle=0,
                # tickcolor='crimson',
                tickfont=dict(
                    size=6,
                    family=font_famliy,
                ),

                # Xaxis line
                linewidth=0.5,
                linecolor='black',
                showline=True,

                # Title properties
                title=dict(
                    standoff=5,
                    font=dict(
                        size=8,
                        family=font_famliy,
                    )
                )

            )
            fig.update_yaxes(
                row=ax_map[(row_i, col_i)]['row'],
                col=ax_map[(row_i, col_i)]['col'],
                automargin=False,
                domain=[y0, y1],

                # Xaxis ticks
                tickmode='array',
                tickvals=[],
                tickwidth=0.5,
                ticklen=0.1,
                # tickcolor='crimson',
                tickfont=dict(
                    size=6,
                    family=font_famliy,
                ),

                # Xaxis line
                linewidth=0.5,
                linecolor='black',
                showline=True,

                # Title properties
                title=dict(
                    standoff=0,
                    font=dict(
                        size=8,
                        family=font_famliy,
                    )
                )
            )




    return fig, ax_map


def make_fig(width, height, x_domains, y_domains, **kwargs) -> go.Figure:
    for i, k in kwargs.items():
        assert i in ['subplot_titles', 'specs', 'equal_width_height', 'equal_width_height_axes'], f'{i}'

    # Check dimensions of x_domains and y_domains
    for row in x_domains.keys():
        assert row in y_domains.keys()
        assert len(x_domains[row]) == len(y_domains[row])
        for i in x_domains[row]:
            assert len(i) == 2
        for i in y_domains[row]:
            assert len(i) == 2

    font_famliy = 'calibri'
    figwidth_pxl = 498 * width
    figheight_pxl = 842 * 0.25 * height
    if 'equal_width_height' in kwargs.keys():
        eqw = kwargs['equal_width_height']
        assert eqw in [None, 'x', 'y', 'shortest']
    else:
        eqw = None

    if 'equal_width_height_axes' not in kwargs.keys():
        eqw_axes = 'all'
    else:
        eqw_axes = kwargs['equal_width_height_axes']

    specs_f = dict()
    if 'shared_xaxes' in kwargs.keys():
        specs_f['shared_xaxes'] = kwargs['shared_xaxes']
    # if 'subplot_titles' in kwargs.keys():
    #     specs_f['subplot_titles'] = kwargs['subplot_titles']
    if 'specs' in kwargs.keys():
        specs_f['specs'] = kwargs['specs']

    # Detect the nr of cols
    n_cols = 0
    for row, specs in x_domains.items():
        n_cols = np.max([n_cols, len(specs)])

    n_cols = np.int(n_cols)
    n_rows = len(x_domains.keys())

    fig = make_subplots(rows=n_rows, cols=n_cols, **specs_f)

    fig.update_layout(
        # Figure dimensions
        autosize=False,
        width=figwidth_pxl,
        height=figheight_pxl,
        margin=dict(l=0, t=0, b=0, r=0),
        plot_bgcolor='white',
        paper_bgcolor='white',
        legend=dict(
            borderwidth=0.5,
            bordercolor='black',
            font=dict(
                family=font_famliy,
                size=6,
            )
        ),
    )

    for row_i in range(n_rows):
        n_cols = len(x_domains[row_i+1])

        for col_i in range(n_cols):

            if 'subplot_titles' in kwargs.keys():
                # print(kwargs['subplot_titles'], ax_tick-1)
                fig.add_annotation(
                    x=x_domains[row_i+1][col_i][0],
                    y=y_domains[row_i+1][col_i][1],
                    text=kwargs['subplot_titles'][row_i+1][col_i],
                    font=dict(size=8, family=font_famliy),
                    showarrow=False,
                    xanchor='left', yanchor='bottom',
                    xref='paper', yref='paper',
                )

            x0, x1 = x_domains[row_i+1][col_i]
            y0, y1 = y_domains[row_i+1][col_i]
            if eqw_axes == 'all' or [row_i + 1, col_i + 1] in eqw_axes:
                if eqw == 'x':
                    n_px_x = (x1-x0) * figwidth_pxl
                    dy = n_px_x / figheight_pxl
                    y1 = y0 + dy
                elif eqw == 'y':
                    n_px_y = (y1-y0) * figheight_pxl
                    dx = n_px_y / figwidth_pxl
                    x1 = x0 + dx

            fig.update_xaxes(
                row=row_i+1,
                col=col_i+1,
                # automargin=False,
                domain=[x0, x1],

                # Xaxis ticks
                tickmode='array',
                tickvals=[],
                tickwidth=0.5,
                ticklen=0.1,
                tickangle=0,
                # tickcolor='crimson',
                tickfont=dict(
                    size=6,
                    family=font_famliy,
                ),

                # Xaxis line
                linewidth=0.5,
                linecolor='black',
                showline=True,

                # Title properties
                title=dict(
                    standoff=5,
                    font=dict(
                        size=8,
                        family=font_famliy,
                    )
                )

            )
            fig.update_yaxes(
                row=row_i+1,
                col=col_i+1,
                automargin=False,
                domain=[y0, y1],

                # Xaxis ticks
                tickmode='array',
                tickvals=[],
                tickwidth=0.5,
                ticklen=0.1,
                # tickcolor='crimson',
                tickfont=dict(
                    size=6,
                    family=font_famliy,
                ),

                # Xaxis line
                linewidth=0.5,
                linecolor='black',
                showline=True,

                # Title properties
                title=dict(
                    standoff=0,
                    font=dict(
                        size=8,
                        family=font_famliy,
                    )
                )
            )

    return fig


def simple_fig(equal_width_height=None):
    fig = make_fig(
        width=1, height=1,
        x_domains={
            1: [[0.1, 0.9]],
        },
        y_domains={
            1: [[0.1, 0.9]]
        },
        equal_width_height=equal_width_height,
    )
    return fig


def get_dataset_trial_ids(sid=None) -> pd.DataFrame:
    """"
    Select trials based on criteria and save in "dataset_trial_ids"
    """
    # If this was allready computed, do not recompute
    if is_saved('dataset_trial_ids'):
        trial_df = load_data('dataset_trial_ids')['trial_df']

    else:
        min_duration = 200*qt.ms  # Trial should have at least 200 ms of sample time
        max_duration = 800*qt.ms  # Max sampling duration should be 800 ms
        max_duration_trial = 4500*qt.ms  # Max duration of a trial (sample start to poke) should be 4s

        events_of_interest = ['trial_start', 'sample_start', 'retraction', 'poke']  # events needed for analysis
        all_tids = True
        no_noise_trials = True

        # Open data connection
        io = Io()
        trial_df = pd.DataFrame()
        for sid in prm.session_ids:
            io.load_session(sid)

            trial_ids = io.select_trials(
                all=no_noise_trials,  # out of all trials
                no_noise_trials=all_tids,  # remove the noisy LFP trials
                events_of_interest=events_of_interest,  # These events need to be there,
            )

            sample_start = io.get_event_times(trial_ids=trial_ids, event_name='sample_start')
            retraction = io.get_event_times(trial_ids=trial_ids, event_name='retraction')
            poke = io.get_event_times(trial_ids=trial_ids, event_name='poke')

            # Remove trials which do not match the sampling epoch duration
            dT = (retraction - sample_start).rescale('ms').magnitude.flatten()
            dT_trial = (poke - sample_start).rescale('ms').magnitude.flatten()

            idx = np.where(
                (dT >= min_duration.magnitude) & (dT <= max_duration.magnitude) &
                (dT_trial <= max_duration_trial.magnitude)
            )[0]

            # Add placeholder for output
            trial_ids_to_keep = []

            # Add select trials to the to_keep list
            for i in idx:
                trial_ids_to_keep.append(trial_ids[i])

            df = io.trial_df
            df['session_id'] = sid
            trial_df = pd.concat([trial_df, df.loc[trial_ids_to_keep]])

        save_data(savename='dataset_trial_ids', data=dict(trial_df=trial_df))

    if sid is None:
        return trial_df
    else:
        return trial_df.loc[trial_df.session_id == sid]


def get_unit_ids(sid=None) -> pd.DataFrame:
    # Find all unit ids to include for analysis

    loadname = 'unit_ids'
    if is_saved(loadname):
        unit_ids = load_data(loadname)['unit_ids']
    else:
        io = Io()

        unit_ids = pd.DataFrame()
        for sid in prm.session_ids:
            print(sid)
            io.load_session(sid)
            io.set_unit_criteria(**prm.unit_quality_thresholds, verbose=False)
            uids = io.select_units(all=True, good_unit=True)
            df = io.unit_df
            df['session_id'] = sid
            unit_ids = pd.concat([unit_ids, df.loc[uids]])

        save_data(data=dict(unit_ids=unit_ids), savename=loadname)

    if sid is None:
        return unit_ids
    else:
        return unit_ids.loc[unit_ids.session_id == sid]


def get_lfp_ids(sid=None) -> pd.DataFrame:
    # Find all lfp ids to include for analysis
    # loadname = 'lfp_ids'
    # if is_saved(loadname):
    #     lfp_ids = load_data(loadname)['lfp_ids']
    # else:
    #     io = Io()
    #     lfp_ids = pd.DataFrame()
    #     for sid in prm.session_ids:
    #         print(sid)
    #         io.load_session(sid, read_lfp=True)
    #         lids = io.select_lfps(clean=False, best_on_tetrode=True)
    #         df = io.lfp_df
    #         df['session_id'] = sid
    #         lfp_ids = pd.concat([lfp_ids, df.loc[lids]])
    #         print(lfp_ids.shape)
    #
    #     save_data(data=dict(lfp_ids=lfp_ids), savename=loadname)
    #
    # if sid is None:
    #     return lfp_ids
    # else:
    #     return lfp_ids.loc[lfp_ids.session_id == sid]

    lid_file = Path(r'G:\data-vita\selected_leads.xlsx')
    if not lid_file.is_file():
        print('update path to lid file in tools.py')

    leads = pd.read_excel(lid_file, engine='openpyxl',
                          index_col=0)

    if sid is None:
        return leads
    else:
        return leads.loc[sid]


def cut_asig_by_event(sig: neo.AnalogSignal, t_left: qt.Quantity, t_right: qt.Quantity) -> np.ndarray:
    # Cut an neo.Analogsignal using the t_left and t_right boundaries
    # t_left and t_right are timeonset and ofset for epoch
    srate = sig.sampling_rate.magnitude
    N_trials = len(t_left)
    dT = (t_right[0] - t_left[0]).rescale('s').magnitude
    N_datapts = int(dT * srate)
    data = np.zeros((N_trials, N_datapts))

    for ti, (tl, tr) in enumerate(zip(t_left, t_right)):
        d = sig.time_slice(tl, tr).magnitude.flatten()

        # Deal with rounding errors in the neo toolbox
        if len(d) == N_datapts:
            data[ti, :] = d
        elif 0 < N_datapts - len(d) < 2:
            data[ti, :len(d)-1] = d
        elif -2 < N_datapts - len(d) < 0:
            data[ti, :] = d[:-1]
        else:
            raise ValueError('Something wrong with dimensions')

    return data


def run_job(job_fn, n_proceses, joblist):
    """
    Run a function in parallel

    :param job_fn: function to run the job on
    :param n_proceses: nr of parallel processes
    :param joblist: list of lists with input values for the function
    :return:
    """
    pool = mp.Pool(processes=n_proceses)
    for job in joblist:
        pool.apply_async(job_fn, args=job)

    # Call pool.close() and pool.join(), otherwise the main script will not wait for apply_async to
    # finish and kill all workers
    pool.close()
    pool.join()


def sort_heatmap(heatmap):
    maxidx = np.argmax(heatmap, axis=1)
    return heatmap[np.argsort(maxidx)[::-1], :]


if __name__ == '__main__':
    # get_dataset_trial_ids()
    _ = get_lfp_ids()