from pathlib import Path
from plotly.colors import diverging as div
from plotly.colors import sequential as seq
import numpy as np

data_dir = Path(r'D:\data_paul')  # Path where you saved the dataset (only for prepocessing
dataset_dir = Path(r'D:\data_paul\dataset') # Path where you downloaded the dataset

# dataset_dir = Path(r'C:/users/downloads')
result_dir = Path(r'G:\sustained_firing_prh')
dump_dir = result_dir / 'dump'
session_overview_file = dataset_dir / 'misc' / 'session_overview.csv'

# Colors on how to mark events in your analysis
event_colors = {
    'BeamFR_off': seq.YlOrBr[-4],
    'BeamFL_off': seq.YlOrRd[-4],
    'BeamBC_off': seq.YlOrRd[2],
    'LLick Start': seq.PuBu[-4],
    'RLick Start': seq.PuBuGn[-4],
    'CLick Start': seq.YlGnBu[-6],
    'RAccess Start': seq.Purples[-3],
    'LAccess Start': seq.BuPu[-2],
    'CAccess Start': seq.Burg[-2],
}

area_colours = dict(
    V='#EA6077',
    P='#127C2D',
    A='#476DAB',
)

side_colors = {
    'L': '#F45B69',
    'left': '#F45B69',
    'R': '#114B5F',
    'right': '#114B5F',
    'middle': '#F5F749',
}

choice_colors = {
    'L': '#F45B69',
    'R': '#114B5F'
}
modality_colors = {
    'M': '#d916d6',
    'A': '#2da031',
    'V': '#d91616',
}

correct_colors = {
    False: '#6F1A07',
    True: '#379634',
}
area_abbreviations = dict(P='PRH', A='AUD', V='VIS')

camera_resolution = np.sqrt(4.2) / 10  # mm / px

left_arm_bins = [10, 15]
right_arm_bis = [38, 45]

landmark_bins = dict(
    FR=14, BC=46, FC=58, FL=72,
)



