from .base_io import Io as base_io
import src.config as cfg
from pathlib import Path
import pandas as pd
import pickle
from src import tools
import quantities as qt
import numpy as np
import scipy
import neo



class Io(base_io):
    stage = None

    def __init__(self, path=cfg.dataset_dir, read_lfp=False):
        """

        Parameters
        ----------
        path
            Directory wherefrom to read the dataset (don't pass to read default directory)
        read_lfp
            Whether the io should also read to lfp files (faster if not). read_lfp can also
            be passed to load_session
        """

        # Whole io now works with pathlib, check path datatype and check if path exists
        if not isinstance(path, Path):
            path = Path(path)

        if not path.is_dir():
            raise ValueError(f'path {path}" does not exist')

        self.read_lfp = read_lfp  # Wheter to read lfp files or not during 'load_session' call
        self.dataset_path = path  # Path containing the data

        # Read sessions frame
        self.sessions_df = pd.read_csv((self.dataset_path / 'session_overview.csv').as_posix(),
                                       index_col=0)
        self.sessions_df.index = self.sessions_df.date
        session_ids = self.sessions_df.date.to_list()

        self.session_ids = [int(sid) for sid in session_ids]

    def get_object(self, object_id):
        if 'lfp' in object_id:
            if 'clean' in object_id:
                channel_index = int(self.lfp_clean_df.loc[object_id].channel_idx)
                lfps = self.block.filter(name='lfps_clean')[0]
                return lfps.load(time_slice=None, channel_indexes=[channel_index])
            else:
                channel_index = int(self.lfp_df.loc[object_id].channel_idx)
                lfps = self.block.filter(name='lfps')[0]
                return lfps.load(time_slice=None, channel_indexes=[channel_index])

        elif object_id == 'binned_maze' or object_id == 'linearized_maze':
            savename = f'binned_maze/{self.session_id}_binned_maze_inverted'

            if not tools.is_saved(savename):
                raise ValueError(f'Binned maze nog saved, make sure to run "get_maze_bins" for this session {savename}')

            data = tools.load_data(savename)['bins']
            return data

        elif object_id == 'binned_maze_paul':
            savename = f'binned_maze_paul/{self.session_id}_binned_maze'
            if not tools.is_saved(savename):
                self._get_binned_maze_paul()

            return tools.load_data(savename)['df']

        elif object_id == 'head_linearized':
            savename = f'{self.session_id}_head_linearized'
            ld = tools.load_data(savename)
            return ld['head_pos']

        elif object_id == 'body_linearized':
            savename = f'{self.session_id}_body_linearized'
            ld = tools.load_data(savename)
            return ld['body_pos']

        else:
            res = self.block.filter(name=object_id)
            if len(res) == 0:
                raise ValueError(f'{object_id} returned no objects')
            elif len(res) > 1:
                raise ValueError(f'{object_id} returned multiple objects')
            return res[0]

    def _detect_session(self, object_id):
        sid = None

        if 'sp' in object_id:
            for s in self.session_ids:
                if f'{s}' in object_id:
                    sid = s
                    break

        assert sid is not None, f'could not match {object_id}'
        return sid

    def load_session(self, object_id, read_lfp=False):
        if isinstance(object_id, str) and 'sp' in object_id:
            sid = self._detect_session(object_id)
        else:
            sid = object_id
        base_io.load_session(self, sid, read_lfp)
        self.stage = self.sessions_df.loc[self.session_id].stage

    def _get_binned_maze_paul(self):
        pos = self.get_object('binned_position')
        lmap = pd.DataFrame()

        s_map = {
            0: 'right',
            1: 'left',
            2: 'middle',
        }

        all_pos = np.unique(pos.magnitude)
        all_pos = all_pos[pd.notna(all_pos)]
        for p in all_pos:
            idx = np.where(pos.magnitude.flatten() == p)[0]
            s = pos.annotations['side'][idx[0]]
            lmap.at[p, 'side'] = s_map[s]

        tools.save_data(data=dict(df=lmap),
                        savename=f'binned_maze_paul/{self.session_id}_binned_maze')

    @staticmethod
    def get_firing_rate(spiketrain, t_start=None, t_stop=None):
        """
        this function is based on the elephant.statistics.instantaneous_firingrate
        """
        units = qt.ms  # sample the firing rate at 1 ms resolution
        cutoff = 5  # nr standard deviations
        sigma = 150  # ms

        # Set the spiketrain units in ms
        spiketrain = spiketrain.rescale(units)

        # Find the time start and stop intervals:
        # either the full train or cut by specified interval
        if t_start is None:
            t_start = spiketrain.t_start
        else:
            t_start = t_start.rescale(spiketrain.units)

        if t_stop is None:
            t_stop = spiketrain.t_stop
        else:
            t_stop = t_stop.rescale(spiketrain.units)

        # Create a zero vector to count the spikes
        time_vector = np.zeros(int((t_stop - t_start)) + 1)

        # Extract the spikes in the interval to analyse
        spikes_slice = spiketrain.time_slice(t_start, t_stop) \
            if len(spiketrain) else np.array([])

        # Assign each spike to a timepoint
        for spike in spikes_slice:
            index = int((spike - t_start))
            time_vector[index] += 1

        # Create a kernel for convolution
        t_arr = np.arange(-cutoff * sigma, cutoff * sigma + 1, 1)  # time stamps
        kernel = scipy.stats.norm.pdf(t_arr, loc=0, scale=sigma)  # gaussian kernel
        n_kernel = int(kernel.size / 2)  # size of the kernel

        r = scipy.signal.fftconvolve(time_vector,kernel, 'full') * 1000  # compute the rate,
            # multiply by 1000 to convert to seconds

        # Cutoff the edges
        r = r[kernel.size:-kernel.size]

        # export as a neo.analogsignal
        rate = neo.AnalogSignal(
            signal=r, sampling_period=1 * qt.ms, units=qt.Hz, t_start=t_start + n_kernel * qt.ms,
            t_stop=t_stop - n_kernel * qt.ms
        )
        return rate

    @staticmethod
    def get_signal_at_times(signal, times):
        signal_values = signal.magnitude.flatten()
        signal_times = signal.times.rescale('ms').magnitude.flatten()
        times = times.rescale('ms').magnitude.flatten()

        sig_out = np.zeros(times.size)
        for ti, t in enumerate(times):
            idx = np.argmin(np.abs(signal_times - t))
            sig_out[ti] = signal_values[idx]

        return neo.IrregularlySampledSignal(
            signal=sig_out,
            times=times,
            units=signal.units,
            time_units=qt.ms,
        )


if __name__ == '__main__':
    x = Io()
    x.load_session(x.session_ids[0])
    r = x.get_object('linearized_maze')