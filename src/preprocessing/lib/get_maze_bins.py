from src import Io, cfg, tools
import numpy as np
import pandas as pd
from scipy.signal import convolve2d
import math
from scipy.interpolate import interp1d
import cv2
import scipy.misc


markers = [
    'trial_start',
    'first_l_lick',
    'first_r_lick',
    'BeamBC_on'
]

current_vid = None
current_idx = 0
is_saved = False
is_finished = False
side = None
path = pd.DataFrame()


def click_event(ev, x, y, flags, params):
    global current_idx, current_vid, path, is_saved, is_finished, side
    if ev == cv2.EVENT_LBUTTONDOWN:
        path.at[current_idx, 'x'] = int(x)
        path.at[current_idx, 'y'] = int(y)
        path.at[current_idx, 'side'] = side
        current_idx += 1
        is_saved = True

    elif ev == cv2.EVENT_RBUTTONDOWN:
        is_finished = True


def get_maze_bins(sid: str):
    global current_vid, path, is_saved, side, is_finished

    if isinstance(sid, int):
        sid = f'{sid}'
    savename = f'binned_maze/{sid}_binned_maze'
    if tools.is_saved(savename):
        return

    print(f'starting: {sid}')
    path = pd.DataFrame()

    io = Io(cfg.dataset_dir)
    io.load_session(sid)

    # Extract tracked position during trials
    d = io.get_object('tracker_paul')  # tracker data
    tracker_times = d.times.rescale('ms').magnitude

    idx = []  # Datapoints occuring during trials
    tstart = io.get_event_times(trial_ids=io.trial_ids,
                                event_name='trial_start').rescale('ms').magnitude
    tstop = io.get_event_times(trial_ids=io.trial_ids,
                               event_name='trial_stop').rescale('ms').magnitude
    for t0, t1 in zip(tstart, tstop):
        idx.append(np.where((tracker_times >= t0) & (tracker_times <= t1))[0])

    idx = np.hstack(idx)
    tracker_x = d.magnitude[idx, 0].astype(int)
    tracker_y = d.magnitude[idx, 1].astype(int)

    assert np.max(tracker_x) < 1000
    assert np.max(tracker_y) < 1000

    frame = np.zeros((1000, 1000, 3))

    for x, y in zip(tracker_x, tracker_y):
        frame[x, y, :] += 1

    side = 'right'
    is_finished = False

    while not is_finished:
        fplot = frame.copy()
        for i, r in path.iterrows():
            fplot = cv2.circle(fplot, (int(r.x), int(r.y)), 5, (255, 0, 0), -1)

        cv2.imshow(side, fplot)
        cv2.setMouseCallback(side, click_event)
        is_saved = False
        while not is_saved:
            cv2.waitKey(100)

        cv2.destroyAllWindows()

    side = 'middle'
    is_finished = False
    while not is_finished:
        fplot = frame.copy()
        for i, r in path.iterrows():
            fplot = cv2.circle(fplot, (int(r.x), int(r.y)), 5, (255, 0, 0), -1)

        cv2.imshow(side, fplot)
        cv2.setMouseCallback(side, click_event)
        is_saved = False
        while not is_saved:
            cv2.waitKey(100)

        cv2.destroyAllWindows()

    side = 'left'
    is_finished = False
    while not is_finished:
        fplot = frame.copy()
        for i, r in path.iterrows():
            fplot = cv2.circle(fplot, (int(r.x), int(r.y)), 5, (255, 0, 0), -1)

        cv2.imshow(side, fplot)
        cv2.setMouseCallback(side, click_event)
        is_saved = False
        while not is_saved:
            cv2.waitKey(100)

        cv2.destroyAllWindows()

    # Interpolate the path between the path points
    x = np.arange(0, path.shape[0])
    xfit = np.arange(0, path.shape[0]-1, 0.001)
    fx = interp1d(x, path.x.values, kind='linear')
    pxfit = fx(xfit)

    fy = interp1d(x, path.y.values, kind='linear')
    pyfit = fy(xfit)

    d = np.sqrt(np.diff(pxfit)**2 + np.diff(pyfit)**2)
    total_length = np.sum(d)
    n_steps = 50
    stepsize = total_length / n_steps

    # Divide the fit into equally spaced bins
    bins = [[pxfit[0], pyfit[0]]]
    current_i = 0

    while True:
        d = np.sqrt((pxfit - bins[-1][0]) ** 2 + (pyfit - bins[-1][1]) ** 2)
        idx_step = np.where(d >= stepsize)[0]
        idx = np.where(idx_step > current_i)[0]
        if len(idx) == 0:
            break

        idx = idx[0]
        bins.append([pxfit[idx_step[idx]], pyfit[idx_step[idx]]])
        current_i = idx_step[idx]

    b = pd.DataFrame()
    for bi, bval in enumerate(bins):
        b.at[bi, 'x'] = bval[0]
        b.at[bi, 'y'] = bval[1]
        b.at[bi, 'trigger'] = ''
        b.at[bi, 'clr'] = tools.interp_color(len(bins), bi, ('cyclical', 'HSV'), 1)

        idx = np.argmin(np.sqrt((path.x.values - bval[0])**2 + (path.y.values - bval[1])**2))
        b.at[bi, 'side'] = path.iloc[idx].side

    tools.save_data(data=dict(bins=b, path=path), savename=savename)

    print(f'savend: {savename}')


# def rebin_map(sid):


def invert_bins(sid):
    # X and Y are swapped for some reason in the annotated data
    savename = f'binned_maze/{sid}_binned_maze_inverted'
    if tools.is_saved(savename):
        return

    loadname = f'binned_maze/{sid}_binned_maze'
    ld = tools.load_data(loadname)['bins']
    bins_out = pd.DataFrame(index=ld.index)
    bins_out['x'] = ld['y']
    bins_out['y'] = ld['x']
    bins_out['side'] = ld['side']
    bins_out['clr'] = ld['clr']

    tools.save_data(
        data=dict(bins=bins_out),
        savename=savename,
    )




#
if __name__ == '__main__':
    invert_bins('20151217')


