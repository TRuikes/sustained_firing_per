import neo
import src.config as cfg
import pandas as pd
import scipy.io as sio
import numpy as np
import pickle
from src.preprocessing.lib.read_video_smifile import get_videoframes_dataframe

codes = dict(
    modality={
        0: 'A',
        1: 'V',
        2: 'M',
    },
    trial_type={
        0: 'threshold',
        1: 'suprathreshold',
        2: 'multi',
        3: 'all',
    },
    cue={
        0: 'Left',  # Left
        1: 'Right',  # Right
        2: 'Both',  # Both
    },
    choice={
        0: 'Left',
        1: 'Right',
        2: 'Both',
        3: 'only correct',
        4: 'only incorrect',
    }
)

visual_stimulus_margin = 0.3  # maximum time in 's' the sensor data for visual stimulus duration can differ from
# the duration annotated in behavioural data


def create_neo(sid):
    print(f'Creating neo for {sid}')
    units_df = pd.read_csv((cfg.dataset_dir / 'misc' / f'{sid}_units.csv'))
    events_df = pd.read_csv((cfg.dataset_dir / 'misc' / f'{sid}_events.csv'))
    behaviour_df = pd.read_csv((cfg.dataset_dir / 'misc' / f'{sid}_behaviour.csv'))
    binned_pos_df = pd.read_csv((cfg.dataset_dir / 'misc' / f'{sid}_binned_location.csv'))

    # if (cfg.data_dir / 'Videos' / f'{sid}_tdata_mapped_to_smi.csv').is_file():
    #     has_mapped_frames = True
    #     location_df = pd.read_csv((cfg.data_dir / 'Videos' / f'{sid}_tdata_mapped_to_smi.csv'),
    #                               index_col=0, header=[0, 1])
    # else:
    #     location_df = pd.read_csv((cfg.dataset_dir / 'misc' / f'{sid}_location.csv'),
    #                               )
    #     has_mapped_frames = False
    bonsai_data = pd.read_csv((cfg.dataset_dir / 'misc' / f'{sid}_location.csv'))

    block = neo.Block()
    block.segments.append(neo.Segment())

    # Convert the behavioural numerical codes in Behaviour to readable
    # strings
    for k in codes['cue'].keys():
        behaviour_df.loc[behaviour_df.cue == k, 'cue'] = codes['cue'][k]

    for k in codes['modality'].keys():
        behaviour_df.loc[behaviour_df.modality == k, 'modality'] = codes['modality'][k]

    for k in codes['choice'].keys():
        behaviour_df.loc[behaviour_df.choice == k, 'choice'] = codes['choice'][k]

    # Convert the original trial number to unique strings for the dataset
    trial_nr_map = dict()
    trial_nrs_original = events_df.trial_nr.unique()
    for ti, tnr in enumerate(trial_nrs_original):
        trial_nr_map[tnr] = f'tid_{sid}_{ti:03d}'

    # Use a temporary trial id to later demarcate my own trial ids
    # Instead of using the ITI start as a trial start, I'll use the nose-poke
    is_first_trial = True
    for i, r in events_df.iterrows():
        curr_tid = trial_nr_map[r.trial_nr]
        if is_first_trial:
            prev_tid = curr_tid
            events_df.at[i, 'new_trial'] = True

        # If the previous row had a different trial id, this is a new trial!
        if curr_tid != prev_tid:
            events_df.at[i, 'new_trial'] = True

        # Other wise its not (Except if it is the very first trial)
        elif not is_first_trial:
            events_df.at[i, 'new_trial'] = False

        # During the first trial, the previous statements are not true,
        # and is_first_trial is now set to False
        elif is_first_trial:
            is_first_trial = False

        events_df.at[i, 'tid_paul'] = curr_tid  # mark this as trial numbering by paul
        prev_tid = curr_tid

    # Find the stilus onset in LEFT and RIGHT side trials
    stimulus_times = pd.DataFrame()
    stimulus_start_left = events_df.loc[events_df.event_name == 'Left Visual ON'].copy()
    stimulus_end_left = events_df.loc[events_df.event_name == 'Left Visual OFF'].copy()

    for _, r in stimulus_start_left.iterrows():
        idx = r.tid_paul
        stimulus_times.at[idx, 'start'] = r.event_ts
        stimulus_times.at[idx, 'stimulus_side'] = 'left'
        stimulus_times.at[idx, 'tid_paul'] = r.tid_paul
        stimulus_times.at[idx, 'ev_idx'] = r.name
        stimulus_times.at[idx, 'cue'] = codes['cue'][r.direction]
        stimulus_times.at[idx, 'trialtype'] = codes['trial_type'][r.trial_type]
        stimulus_times.at[idx, 'correct'] = r.correct
        stimulus_times.at[idx, 'modality'] = codes['modality'][r.modality]
        stimulus_times.at[idx, 'choice'] = codes['choice'][r.choice]

        if r.tid_paul in stimulus_end_left.tid_paul.values:
            df = stimulus_end_left.query('tid_paul == @idx')
            # assert df.shape[0] == 1, df.shape
            stimulus_times.at[idx, 'end'] = df.event_ts.values[0]

    # Now repeat for right side
    stimlus_start_right = events_df.loc[events_df.event_name == 'Right Visual ON'].copy()
    stimlus_end_right = events_df.loc[events_df.event_name == 'Right Visual OFF'].copy()

    for _, r in stimlus_start_right.iterrows():
        assert r.tid_paul not in stimulus_start_left.index
        idx = r.tid_paul
        stimulus_times.at[idx, 'start'] = r.event_ts
        stimulus_times.at[idx, 'stimulus_side'] = 'left'
        stimulus_times.at[idx, 'tid_paul'] = r.tid_paul
        stimulus_times.at[idx, 'ev_idx'] = r.name
        stimulus_times.at[idx, 'cue'] = codes['cue'][r.direction]
        stimulus_times.at[idx, 'trialtype'] = codes['trial_type'][r.trial_type]
        stimulus_times.at[idx, 'correct'] = r.correct
        stimulus_times.at[idx, 'modality'] = codes['modality'][r.modality]
        stimulus_times.at[idx, 'choice'] = codes['choice'][r.choice]

        if r.tid_paul in stimlus_end_right.tid_paul.values:
            df = stimlus_end_right.query('tid_paul ==@idx')
            # assert df.shape[0] == 1
            stimulus_times.at[idx, 'end'] = df.event_ts.values[0]

    stimulus_times = stimulus_times.sort_values('start')

    ev_names = events_df.query('new_trial == True').event_name.unique()
    print(f'triggeres marked as trial start by Paul: ', ev_names)

    # Make sure the number of detected tials in the events, matches the number of
    # trials in the behavioural data
    tmp_trial_start_df = events_df.query('new_trial == True')
    n_trials_ttl = tmp_trial_start_df.shape[0]
    n_trials_behaviour = behaviour_df.shape[0]
    n_trials_visual_start = stimulus_times.shape[0]
    assert n_trials_ttl == n_trials_behaviour, f'Number of trials in ttl data should match' \
                                               f'the number of trials in behavioural data'

    # Make sure there are an equal amount of trials marked by PAul,
    # as there are stimulus onsets
    if isinstance(sid, int):
        sid = f'{sid}'

    if sid not in ['20151223', '20160424', '20160426', '20160521', '20160523']:
        assert n_trials_visual_start == n_trials_ttl, f'{n_trials_visual_start}, {n_trials_ttl}'
    elif sid in ['20151223', '20160424', '20160426', '20160521', '20160523']:
        print(f'\t{sid}: mismatch of event df trials number and behavioural annotations trial nr')

    # Map new trial numbering to events; trial start is marked by stimulus onset
    events_df['tid'] = None

    # Map the entries of the behavioural frame to the start times (assume
    # a 1 to 1 mapping)
    trial_start = neo.Event(
        name='trial_start',
        times=stimulus_times.start.values / 10,
        units='ms',
        labels=stimulus_times.index.values,
        array_annotations=dict(
            modality=stimulus_times.modality.values,
            choice=stimulus_times.choice.values,
            correct=stimulus_times.correct.values,
            cue=stimulus_times.cue.values,
            trialtype=stimulus_times.trialtype.values,
        )
    )
    block.segments[0].events.append(trial_start)

    # Get trial stop times
    trial_stop_times = np.roll(stimulus_times.start.values / 10, -1)
    trial_stop_times[-1] = trial_start.times[-1].magnitude + 20000
    trial_stop = neo.Event(
        name='trial_stop',
        times=trial_stop_times.astype(float),
        units='ms',
        labels=stimulus_times.index.values,
    )
    block.segments[0].events.append(trial_stop)

    # Add visual starts as neo events
    block.segments[0].events.append(neo.Event(
        name='visual_start',
        times=stimulus_times.start.values / 10,
        units='ms',
        labels=stimulus_times.index.values,
        array_annotations=dict(side=stimulus_times.stimulus_side.values)
        # side=visual_stim.side.values
        # table_index=visual_start.index.values,
    ))
    # Add visual end as neo event
    block.segments[0].events.append(neo.Event(
        name='visual_stop',
        times=stimulus_times.end.values / 10,
        units='ms',
        labels=stimulus_times.index.values,
        array_annotations=dict(side=stimulus_times.stimulus_side.values)
        # table_index=visual_start.index.values
    ))

    # Write all events in event_df as a neo.event
    for evname in events_df.event_name.unique():
        # if 'Start' in evname or 'End' in evname or 'Unknown' in evname:
        #     if 'Lick' not in evname and 'Access' not in evname and 'Pump' not in evname:
        #         continue
        df_event = events_df.query('event_name == @evname')
        new_event = neo.Event(
            name=evname,
            times=df_event.event_ts.values / 10,
            units='ms',
            labels=df_event.tid.values,
            array_annotations=dict(
                ttl=df_event.ttl.values,
            )
        )
        block.segments[0].events.append(new_event)

    # # Detect first lick per trial (NPR)
    first_lick_times = []
    first_lick_labels = []
    first_lick_side = []

    last_lick_times = []
    last_lick_labels = []
    last_lick_side = []

    first_l_lick_times = []
    first_l_lick_labels = []
    last_l_lick_times = []
    last_l_lick_labels = []

    first_r_lick_times = []
    first_r_lick_labels = []
    last_r_lick_times = []
    last_r_lick_labels = []


    r_licks = None
    l_licks = None
    for ev in block.segments[0].events:
        if ev.name == 'RLick Start':
            r_licks = ev
        elif ev.name == 'LLick Start':
            l_licks = ev

    for tid in trial_start.labels:
        if tid in r_licks.labels:
            idx = np.where(r_licks.labels == tid)[0]
            rtime = r_licks.times.rescale('ms').magnitude[idx[0]]
            rtime_last = r_licks.times.rescale('ms').magnitude[idx[-1]]
        else:
            rtime = None

        if tid in l_licks.labels:
            idx = np.where(l_licks.labels == tid)[0]
            ltime = l_licks.times.rescale('ms').magnitude[idx[0]]
            ltime_last = l_licks.times.rescale('ms').magnitude[idx[-1]]
        else:
            ltime = None

        # if rtime is None and ltime is None:
        #     first_lick_times.append(None)
        #     first_lick_labels.append(tid)
        #     first_lick_side.append(None)
        if rtime is None and ltime is not None:
            first_lick_times.append(ltime)
            first_lick_labels.append(tid)
            first_lick_side.append('L')

            last_lick_times.append(ltime_last)
            last_lick_labels.append(tid)
            last_lick_side.append('L')

        elif rtime is not None and ltime is None:
            first_lick_times.append(rtime)
            first_lick_labels.append(tid)
            first_lick_side.append('R')

            last_lick_times.append(rtime_last)
            last_lick_labels.append(tid)
            last_lick_side.append('R')

        elif rtime is not None and ltime is not None:
            i = np.argmin([ltime, rtime])
            first_lick_times.append([ltime, rtime][i])
            first_lick_labels.append(tid)
            first_lick_side.append(['L', 'R'][i])

            i = np.argmax([ltime_last, rtime_last])
            last_lick_times.append([ltime_last, rtime_last][i])
            last_lick_labels.append(tid)
            last_lick_side.append(['L', 'R'][i])

        if ltime is not None:
            first_l_lick_times.append(ltime)
            first_l_lick_labels.append(tid)
            last_l_lick_times.append(ltime_last)
            last_l_lick_labels.append(tid)
        if rtime is not None:
            first_r_lick_times.append(rtime)
            first_r_lick_labels.append(tid)
            last_r_lick_times.append(rtime_last)
            last_r_lick_labels.append(tid)

    # Add first lick as event
    block.segments[0].events.append(neo.Event(
        name='first_lick',
        times=first_lick_times,
        units='ms',
        labels=first_lick_labels,
        lick_side=first_lick_side
    ))

    block.segments[0].events.append(neo.Event(
        name='last_lick',
        times=last_lick_times,
        units='ms',
        labels=last_lick_labels,
        lick_side=last_lick_side
    ))

    block.segments[0].events.append(neo.Event(
        name='first_l_lick',
        times=first_l_lick_times,
        units='ms',
        labels=first_l_lick_labels,
    ))

    block.segments[0].events.append(neo.Event(
        name='last_l_lick',
        times=last_l_lick_times,
        units='ms',
        labels=last_l_lick_labels,
    ))

    block.segments[0].events.append(neo.Event(
        name='first_r_lick',
        times=first_r_lick_times,
        units='ms',
        labels=first_r_lick_labels,
    ))

    block.segments[0].events.append(neo.Event(
        name='last_r_lick',
        times=last_r_lick_times,
        units='ms',
        labels=last_r_lick_labels,
    ))

    # Detect first return to middle

    # A SIGIFICANT AMOUNT OF BEAM_BC IS MISSING
    # LETS NOT USE IT FOR 'RETURN TO MIDDLE' FOR NOW
    # rtm = None
    # for ev in block.segments[0].events:
    #     if ev.name == 'BeamBC_on':
    #         rtm = ev
    #
    # rtm_times = []
    # rtm_labels = []
    # n_rtm_missing = 0
    # for tid in trial_start.labels:
    #     if tid in rtm.labels:
    #         idx = np.where(rtm.labels == tid)[0]
    #         rtm_time = rtm.times.rescale('ms').magnitude[idx[0]]
    #         rtm_times.append(rtm_time)
    #         rtm_labels.append(tid)
    #     else:
    #         n_rtm_missing += 1
    #
    # print(f'\tn rtm missing: {n_rtm_missing}')
    #
    # block.segments[0].events.append(neo.Event(
    #     name='return_to_middle',
    #     times=rtm_times,
    #     units='ms',
    #     labels=rtm_labels,
    # ))

    # Add spiketrains
    for i, r in units_df.iterrows():
        sp_file = cfg.dataset_dir / 'misc' / f'{sid}-{r["name"]}.mat'
        spiketrain = sio.loadmat(sp_file.as_posix())
        spiketimes = spiketrain['times'] / 10

        # There are a lot of 0 entries in the spiketrins
        idx = np.where(spiketimes > 0)[0]
        spiketimes = spiketimes[idx]
        sp = neo.SpikeTrain(
            name=f'sp_{sid}_{r["name"]}',
            units='ms',
            times=spiketimes,
            t_stop=spiketimes[-1]+10,
        )
        for c in r.keys():
            if c == 'name':
                continue

            sp.annotate(**{c: r[c]})
        block.segments[0].spiketrains.append(sp)

    # # Add location data
    tracker_times = bonsai_data.ts.values / 10
    bonsai_cols = ['body_x', 'body_y', 'head_x', 'head_y']
    tracker_sig = neo.IrregularlySampledSignal(
        name='tracker_paul',
        times=tracker_times,
        time_units='ms',
        units='Hz',
        signal=bonsai_data[bonsai_cols].to_numpy(),
        row_names=['body_x', 'body_y', 'head_x', 'head_y'],
        labels=['xxxxxxxxxxxxxxxxxxxx' for _ in range(bonsai_data.shape[0])]
    )

    # Compute the animal speed and add it
    cam_resolution = 2.05  # mm per pixel

    # get position in mm
    bx = bonsai_data['body_x'].values * cam_resolution
    by = bonsai_data['body_y'].values * cam_resolution

    # Displacement in pixels between frames (mm per frame)
    d_px = np.sqrt(np.diff(bx, prepend=np.nan) ** 2 +
                   np.diff(by, prepend=np.nan) ** 2)

    # Get trackertime in seconds
    dt = np.diff(tracker_times / 1000, prepend=np.nan)

    # convert to speed [mm / s]
    speed = d_px / dt

    # convert to metric
    resolution = 2.05  # mm per pixel

    speed = neo.IrregularlySampledSignal(
        name='speed',
        times=tracker_times,
        time_units='ms',
        units='mm/s',
        signal=speed*resolution,
        labels=['xxxxxxxxxxxxxxxxxxxx' for _ in range(bonsai_data.shape[0])]
    )

    # Add headangle data
    body_vector_x = bonsai_data.head_x.values - bonsai_data.body_x.values
    body_vector_y = bonsai_data.head_y.values - bonsai_data.body_y.values
    headangle_rad = np.arctan2(body_vector_x, body_vector_y)
    headdirection = neo.IrregularlySampledSignal(
        name='headdirection',
        times=tracker_times,
        time_units='ms',
        units='radians',
        signal=headangle_rad,
        labels=['xxxxxxxxxxxxxxxxxxxx' for _ in range(bonsai_data.shape[0])]

    )

    for t_start, t_stop, label in zip(trial_start.times, trial_stop.times, trial_start.labels):
        idx = np.where((tracker_sig.times >= t_start) & (tracker_sig.times < t_stop))[0]
        for i in idx:
            tracker_sig.annotations['labels'][i] = label
            speed.annotations['labels'][i] = label
            headdirection.annotations['labels'][i] = label

    block.segments[0].irregularlysampledsignals.append(tracker_sig)
    block.segments[0].irregularlysampledsignals.append(speed)
    block.segments[0].irregularlysampledsignals.append(headdirection)

    # Load binned position
    binned_pos = neo.IrregularlySampledSignal(
        name='binned_position',
        times=binned_pos_df.ts.values / 10,
        time_units='ms',
        units='ms',
        signal=binned_pos_df.BinPos,
        labels=['xxxxxxxxxxxxxxxxxxxx' for _ in range(binned_pos_df.shape[0])],
        side=binned_pos_df.Side.values,
    )
    for t_start, t_stop, label in zip(trial_start.times, trial_stop.times, trial_start.labels):
        idx = np.where((binned_pos.times >= t_start) & (binned_pos.times < t_stop))[0]
        for i in idx:
            binned_pos.annotations['labels'][i] = label

    block.segments[0].irregularlysampledsignals.append(binned_pos)

    # Add points marked on maze to file
    ptsfile = cfg.data_dir / 'Videos' / f'{sid}' / f'{sid}_pts_in_vid.csv'
    if ptsfile.is_file():
        ptsdf = pd.read_csv(ptsfile, index_col=0)
        ptsdict = dict()
        for i, r in ptsdf.iterrows():
            ptsdict[r['name']] = dict(x=r.x, y=r.y)
        block.annotate(maze_points=ptsdict)



    block.annotate(trial_metanames=['modality', 'cue', 'duration', 'choice', 'correct'])
    savename = cfg.dataset_dir / f'{sid}.pkl'
    with open(savename.as_posix(), 'wb') as f:
        pickle.dump(block, f, protocol=4)

    savename = cfg.dataset_dir / f'{sid}.pkl'

    with open(savename.as_posix(), 'wb') as f:
        pickle.dump(block, f, protocol=4)


if __name__ == '__main__':
    create_neo('20151223')