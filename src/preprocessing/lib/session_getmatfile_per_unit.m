readname = 'C:\Users\fpga\Desktop\data_paul\mat-files\PaulSpikesBehaviorRatemaps.mat';
load(readname)

spikesavedir = 'C:\Users\fpga\Desktop\data_paul\dataset\misc\';
warning('off')

for session_i = 1:size(PSession, 2)
    t = table();
    table_ticker = 1;

    session_date = PSession(session_i).Date;
    for cell_i = 1:size(PSession(session_i).CellID, 2)
        cellid = PSession(session_i).CellID{cell_i};
        cellid = cellid(1:end-2);


        times = PSession(session_i).SpikeTS{1, cell_i};

        % Write meta info for units to table
        t.name(table_ticker) = {cellid};
        t.nspikes(table_ticker) = size(times,1);
        t.session(table_ticker) = PSession(session_i).Date;
        t.area(table_ticker) = {'P'};
        table_ticker = table_ticker + 1;

        savename = sprintf('%s%i-%s.mat', spikesavedir, session_date, cellid);
        save(savename, 'times');
    end


    for cell_i = 1:size(ASession(session_i).CellID, 2)
        cellid = ASession(session_i).CellID{cell_i};
        cellid = cellid(1:end-2);


        times = ASession(session_i).SpikeTS{1, cell_i};

        % Write meta info for units to table
        t.name(table_ticker) = {cellid};
        t.nspikes(table_ticker) = size(times,1);
        t.session(table_ticker) = ASession(session_i).Date;
        t.area(table_ticker) = {'A'};
        table_ticker = table_ticker + 1;

        savename = sprintf('%s%i-%s.mat', spikesavedir, session_date, cellid);
        save(savename, 'times');
    end


    for cell_i = 1:size(VSession(session_i).CellID, 2)
        cellid = VSession(session_i).CellID{cell_i};
        cellid = cellid(1:end-2);


        times = VSession(session_i).SpikeTS{1, cell_i};

        % Write meta info for units to table
        t.name(table_ticker) = {cellid};
        t.nspikes(table_ticker) = size(times,1);
        t.session(table_ticker) = VSession(session_i).Date;
        t.area(table_ticker) = {'V'};
        table_ticker = table_ticker + 1;

        savename = sprintf('%s%i-%s.mat', spikesavedir, session_date, cellid);
        save(savename, 'times');
    end


    tablesavename = sprintf('%s%i_units.csv', spikesavedir, PSession(session_i).Date);
    writetable(t, tablesavename)
end