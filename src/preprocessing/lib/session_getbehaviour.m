readname = 'D:\data_paul\mat-files\PaulSpikesBehaviorRatemaps.mat';
load(readname)

%%
spikesavedir = 'D:\data_paul\dataset\misc\';
warning('off')

for session_i = 1:size(PSession, 2)
    t = table;
    session_date = PSession(session_i).Date;

    ntrials = PSession(session_i).Behavior.Trials;
    for trial_i = 1:ntrials
        t.cue(trial_i) = PSession(session_i).Behavior.DirectionCued(trial_i);
        t.duration(trial_i) = PSession(session_i).Behavior.TrialDuration(trial_i);
        t.modality(trial_i) = PSession(session_i).Behavior.CueModality(trial_i);
        t.choice(trial_i) = PSession(session_i).Behavior.DirectionChosen(trial_i);
        t.correct(trial_i) = PSession(session_i).Behavior.Correctness(trial_i);

    end
    
    savename = sprintf('%s%i_behaviour.csv', spikesavedir, session_date);
    writetable(t, savename)
end

%%

session_i = 1;
t2 = 
