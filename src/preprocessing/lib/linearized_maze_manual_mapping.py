from src import Io, cfg, tools
import pandas as pd


mapping = {
    20151217: [[55, 97], [0, 45]],
    20151218: [[55, 97], [0, 45]],
    20151219: [[55, 97], [0, 42]],
    20151220: [[55, 97], [0, 45]],
    20151221: [[55, 97], [0, 45]],
    20151222: [[55, 97], [0, 40]],
    20151223: [[56, 97], [0, 46]],
    20160419: [[55, 97], [0, 40]],
    20160420: [[55, 97], [0, 45]],
    20160423: [[0, 1]],
    20160424: [[55, 97], [0, 45]],
    20160425: [[55, 97], [0, 45]],
    20160426: [[55, 97], [0, 40]],
    20160520: [[0, 1]],
    20160521: [[55, 97], [0, 41]],
    20160523: [[0, 1]],
    20160524: [[55, 97], [0, 40]],
}


def align_map(sid):
    loadname = cfg.dataset_dir / 'misc' / f'{sid}_binned_maze'
    if not tools.is_saved(loadname):
        raise ValueError('Make sure to run get_maze_bins and update this script')

    maze_map = tools.load_data(loadname)['bins']
    maze_map['raw_bin_idx'] = maze_map.index.values

    remapped_idx = pd.DataFrame()
    bin_i = 0
    for i_start, i_end in mapping[sid]:
        for i in range(i_start, i_end+1):
            d = maze_map.loc[i]
            for k, v in d.items():
                remapped_idx.at[bin_i, k] = v
            bin_i += 1

    savename = cfg.dataset_dir / 'misc' / f'{sid}_remapped_binned_maze'
    tools.save_data(data=dict(maze_map=remapped_idx), savename=savename)

