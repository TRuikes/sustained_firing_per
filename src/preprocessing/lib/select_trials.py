import pandas as pd
from src import Io, cfg, tools
import quantities as qt


def select_trials():
    max_trial_duration = 40 * qt.s
    io = Io(cfg.dataset_dir)
    tid_df = pd.DataFrame()

    for sid in io.session_ids:
        tids = []
        io.load_session(sid)

        n_trials_tot = len(io.trial_ids)

        trial_start = io.get_object('trial_start')
        visual_start = io.get_object('visual_start')
        # first_lick = io.get_object('first_lick')
        r_lick = io.get_object('RLick Start')
        l_lick = io.get_object('LLick Start')
        beam_bc = io.get_object('BeamBC_on')

        sess_trial_df = io.trial_df.copy()

        for tid, r in sess_trial_df.iterrows():
            include_trial = True
            reason = ''
            has_trial_start = tid in trial_start.labels
            has_visual_start = tid in visual_start.labels
            # has_first_lick = tid in first_lick.labels
            has_beam_bc = tid in beam_bc.labels

            if not has_trial_start:  # Per definition a trial has a trial start.
                include_trial = False
                reason = 'no_trial_start'
            if not has_visual_start:
                include_trial = False
                reason = 'no_visual_start'

            # if not has_first_lick:
            #     include_trial = False
            #     reason = 'no_first_lick'

            if not has_beam_bc:
                include_trial = False
                reason = 'no_beam_bc'

            t0 = io.get_event_times(trial_ids=[tid], event_name='trial_start')[0]
            t1 = io.get_event_times(trial_ids=[tid], event_name='trial_stop')[0]
            dt = (t1 - t0).rescale('s')
            # if dt > max_trial_duration:
            #     include_trial = False
            #     reason = 'long_duration'

            lick_both_sides = tid in r_lick.labels and tid in l_lick.labels

            if include_trial:
                tids.append(tid)

            sess_trial_df.at[tid, 'include'] = include_trial
            sess_trial_df.at[tid, 'reason'] = reason
            sess_trial_df.at[tid, 'lick_both_sides'] = lick_both_sides
            sess_trial_df.at[tid, 'sid'] = f'{sid}'
            sess_trial_df.at[tid, 'animal'] = io.sessions_df.loc[sid].animal

        tid_df = pd.concat([tid_df, sess_trial_df])

    for sid, sid_df in tid_df.groupby('sid'):
        n_trials = sid_df.shape[0]
        print(f'\n{sid}, {n_trials} trials:')

        for g, df in sid_df.groupby('reason'):
            print(f'\t{g} - {df.shape[0]}')
    tools.save_data(data=dict(df=tid_df), savename='selected_trials')


if __name__ == '__main__':
    select_trials()