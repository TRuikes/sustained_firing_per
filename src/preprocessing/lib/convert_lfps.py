import src.config as cfg
import src.tools as tools
from neo.io import NeuralynxIO
import nixio
from neo import NixIO
import neo
import numpy as np


def convert_lfps(sid, overwrite=False):
    savefile = cfg.dataset_dir / f'{sid}.nio'
    if savefile.is_file() and not overwrite:
        return

    reader = NeuralynxIO(dirname=(cfg.data_dir / 'CSC' / sid).as_posix())
    block = reader.read_block(lazy=True)
    anasig = block.segments[0].analogsignals[0]
    load_sig = anasig.load(channel_indexes=[0], magnitude_mode='raw')

    writer = NixIO(filename=savefile.as_posix(), mode='ow')
    fake_block = neo.Block(name='lfpholder',
                           channel_ids=[chid for chid in anasig.array_annotations['channel_ids']],
                           channel_names=[chid for chid in anasig.array_annotations['channel_names']],
                           sig_units=load_sig.units)

    fake_segment = neo.Segment(name='lfpholder')
    fake_block.segments.append(fake_segment)
    anasig_write = neo.AnalogSignal(
        name='lfp',
        signal=np.zeros((load_sig.shape[0], anasig.shape[1]), dtype=load_sig.dtype),
        units=load_sig.units,
        sampling_rate=load_sig.sampling_rate,
        channel_ids=[chid for chid in anasig.array_annotations['channel_ids']],
        channel_names=[chid for chid in anasig.array_annotations['channel_names']],
        nix_name=[chid for chid in anasig.array_annotations['channel_names']],
    )
    fake_block.segments[0].analogsignals.append(anasig_write)
    writer.write_block(fake_block, use_obj_names=True)
    writer.close()

    pb = tools.PBar(text=f'{sid} getting lfps', n_ticks=40, n_counts=anasig.shape[1])

    nio_file = nixio.File.open(savefile.as_posix())
    block = nio_file.blocks[0]

    for i in range(anasig.shape[1]):
        assert block.data_arrays[i].name == f'lfp.{i}'

        load_sig = anasig.load(channel_indexes=[i], magnitude_mode='raw')
        block.data_arrays[i][:] = np.round(load_sig.as_array()).astype(
            load_sig.dtype
        ).flatten()

        pb.print_1()

    nio_file.close()
    pb.close()
    return


if __name__ == '__main__':
    convert_lfps('20151217')