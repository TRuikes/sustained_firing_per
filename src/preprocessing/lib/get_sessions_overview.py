import pandas as pd
import src.config as cfg


def get_sessions_overview():
    if not cfg.session_overview_file.is_file():
        raise Warning('Need to preprocess the data first, run convert_data_to_csv in matlab')

    df = pd.read_csv(cfg.session_overview_file.as_posix())

    for i, r in df.iterrows():
        if (cfg.data_dir / 'CSC' / f'{r.date}').is_dir():
            df.loc[i, 'has_lfp'] = True
        else:
            df.loc[i, 'has_lfp'] = False

        if 'Audio' in r.stage:
            df.at[i, 'stage'] = 'A'
        elif 'Visual' in r.stage:
            df.at[i, 'stage'] = 'V'

    df.to_csv((cfg.dataset_dir / 'session_overview.csv').as_posix())
    return df

if __name__ == '__main__':
    get_sessions_overview()