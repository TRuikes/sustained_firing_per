readname = 'C:\Users\fpga\Desktop\data_paul\mat-files\PaulSpikesBehaviorRatemaps.mat';
load(readname)

%%

spikesavedir = 'C:\Users\fpga\Desktop\data_paul\dataset\misc\';
warning('off')

for session_i = 1:size(PSession, 2)
    t = table;
    t.event_ts = PSession(session_i).events.TS;
    t.ttl = PSession(session_i).events.TTL;
    t.port = PSession(session_i).events.Port';
    t.event_name = PSession(session_i).events.EventName;
    t.trial_nr = PSession(session_i).events.TrialNum;
    t.trial_type = PSession(session_i).events.TrialType;
    t.direction = PSession(session_i).events.Direction;
    t.choice = PSession(session_i).events.DirectionChosen;
    t.correct = PSession(session_i).events.Correctness;
    t.modality = PSession(session_i).events.Modality;
    t.start_iti = PSession(session_i).events.StartOfITI';

    session_date = PSession(session_i).Date;

    savename = sprintf('%s%i_events.csv', spikesavedir, session_date);
    writetable(t, savename)
end

