spikesavedir = 'D:\data_paul\dataset\misc\';
readname = 'D:\data_paul\mat-files\PaulSpikesBehaviorRatemaps.mat';
load(readname)
clc

%% Get location table

for session_i = 1:size(PSession, 2)
    disp(session_i)
    t = table;
    t.ts = PSession(session_i).Location.TFD(:, 1);
    t.body_x = PSession(session_i).Location.TFD(:, 2);
    t.body_y = PSession(session_i).Location.TFD(:, 3);
    t.head_x = PSession(session_i).Location.TFD(:, 4);
    t.head_y = PSession(session_i).Location.TFD(:, 5);

    session_date = PSession(session_i).Date;

    savename = sprintf('%s%i_location.csv', spikesavedir, session_date);
    writetable(t, savename)
end


%% Get linearized position table

for session_i = 1:size(PSession, 2)
    disp(session_i)
    t = table;
    t.ts = PSession(session_i).Linear.TS(:, 1);
    t.BinPos = PSession(session_i).Linear.BinPos;
    t.Side = PSession(session_i).Linear.Side;

    session_date = PSession(session_i).Date;

    savename = sprintf('%s%i_binned_location.csv', spikesavedir, session_date);
    writetable(t, savename)
end
