import re
import pandas as pd
from pathlib import Path

match_str = '<SYNC Start=(?P<ms>\w+)><P Class=ENUSCC>(?P<nlts>\w+)</SYNC>'


def get_videoframes_dataframe(smifile: Path) -> pd.DataFrame:
    vidname = []
    video_annotations = smifile.read_text()
    ms = []
    framenr = []
    nlts = []
    tick = 0
    for ln in video_annotations.split('\n'):
        res = re.match(match_str, ln)
        if res is not None:
            ms.append(int(res['ms']))
            nlts.append(int(res['nlts']))
            vidname.append(smifile.name)
            framenr.append(tick)
            tick += 1

    frame_index = pd.DataFrame(list(zip(ms, nlts, vidname, framenr)), columns=['vid_ms', 'nl_ts', 'vidname', 'framenr'])

    return frame_index


