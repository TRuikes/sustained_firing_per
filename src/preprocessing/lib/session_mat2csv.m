% Create a folder in ~Data_Paul\dataset named 'misc'

readname = 'D:\data_paul\mat-files\PaulSpikesBehaviorRatemaps.mat';
savename = 'D:\data_paul\dataset\misc\session_overview.csv';

%%
load(readname)

%%
t = table();
for i = 1:size(PSession, 2)
    t.animal(i) = PSession(i).Animal;
    t.date(i) = PSession(i).Date;
    t.t_start(i) = PSession(i).events.FirstTS;
    t.t_stop(i) = PSession(i).events.LastTS;
    t.visual_duration(i) = PSession(i).Behavior.VisualDuration;
    t.stage{i} = PSession(i).Stage;
    fprintf('%s', PSession(i).Stage);
end

writetable(t, savename)

