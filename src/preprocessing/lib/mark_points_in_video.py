import cv2
import pandas as pd
import src.config as cfg


current_vid = None
current_idx = 0
current_name = None
is_saved = False
pts = pd.DataFrame()

pts_to_mark = [
    'front_centre', 'front_right', 'back_right', 'back_entre', 'back_left', 'front_left',
    'central_access', 'left_access', 'right_acces',
    'poke_right', 'poke_centre', 'poke_left',
]


def click_event(ev, x, y, flags, params):
    global current_idx, current_vid, current_name, pts, is_saved
    if ev == cv2.EVENT_LBUTTONDOWN:
        pts.at[current_idx, 'name'] = current_name
        pts.at[current_idx, 'x'] = x
        pts.at[current_idx, 'y'] = y
        pts.at[current_idx, 'vidname'] = current_vid
        current_idx += 1
        is_saved = True


def mark_vid(sid):
    global current_vid, current_name, pts, is_saved
    savename = cfg.data_dir / 'Videos' / f'{sid}' / f'{sid}_pts_in_vid.csv'
    if savename.is_file():
        print(f'{savename} exists')
        return

    print(f'Annotating video...')
    vidfile = cfg.data_dir / 'Videos' / f'{sid}' / f'{sid}.mpg'
    assert vidfile.is_file()

    # Read video and get video properties
    cap = cv2.VideoCapture(vidfile.as_posix())  # reader object
    frame_width = int(cap.get(3))  # width of frame
    nframes = cap.get(cv2.CAP_PROP_FRAME_COUNT)  # number frames in video

    cap.set(cv2.CAP_PROP_POS_FRAMES, int((nframes / 8) * 2))
    r, frame = cap.read()

    assert frame is not None

    for pt in pts_to_mark:
        current_name = pt
        print(f'marking {pt}')
        cv2.imshow(pt, frame)
        cv2.setMouseCallback(pt, click_event)
        is_saved = False
        while not is_saved:
            k = cv2.waitKey(100)
            print(k)
            if k == 27:
                print('escape')
                break
        cv2.destroyAllWindows()


    pts.to_csv(savename.as_posix())
    print(f'savend: {savename}')

    return 1

if __name__ == '__main__':
    sid = '20151219'
    mark_vid(sid)