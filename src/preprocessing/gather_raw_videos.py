import party.config as cfg
from party.preprocessing.lib import get_sessions_overview
from pathlib import Path
import shutil


raw_animal_names = dict(
    Judas='X42 Judas',
    Ken='X50 Ken',
    Kierkegaard='X51 Kierkegaard',
    Lucifer='X61 Lucifer'
)

so = get_sessions_overview()

video_extensions = ['.mpg', '.smi', '.nvt']

raw_datapath = Path(r'F:\Data\Cheetah')
vid_save_dir = cfg.data_dir / 'Videos'
assert raw_datapath.is_dir(), f'cannot find raw datapath: {raw_datapath}'

for i, r in so.iterrows():
    animalpath = raw_datapath / raw_animal_names[r.animal]

    d = f'{r.date}'
    sessionpath = animalpath / f'{d[:4]}-{d[4:6]}-{d[6:]}'

    s_out_dir = vid_save_dir / f'{r.date}'

    if not s_out_dir.is_dir():
        s_out_dir.mkdir(parents=True)

    for f in sessionpath.iterdir():
        if f.suffix in video_extensions:
            print(f'copying {f.name}')
            copy_name = s_out_dir / (f.stem + f.suffix)
            shutil.copy(f.as_posix(), copy_name.as_posix())



