import deeplabcut

from party import Io, cfg, tools
from deeplabcut.gui import labeling_toolbox
import numpy as np
dlc_workdir = r'D:\data_paul\dlc'
vid_dir = cfg.data_dir / 'trimmed_videos'
io = Io()

n_vids = 5

vidfiles = [f for f in vid_dir.iterdir()]

# cp = deeplabcut.create_new_project(
#     'fig8-paul', 'thijs', vidfiles,
#     working_directory=dlc_workdir,
#     copy_videos=True,
# )

cp = r'D:\\data_paul\\dlc\\fig8-paul-thijs-2022-11-29\\config.yaml'
# deeplabcut.extract_frames(cp, 'automatic', 'uniform', crop=False, userfeedback=False)

# deeplabcut.label_frames(cp)
# deeplabcut.check_labels(cp, draw_skeleton=False)

# deeplabcut.create_training_dataset(cp, num_shuffles=5)