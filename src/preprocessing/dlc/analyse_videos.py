import os
os.environ["DLClight"] = "True"
import deeplabcut as dlc
from pathlib import Path

# DLC config file
cfg = r'D:\data_paul\dlc\fig8-paul-thijs-2022-11-29\config.yaml'

vid_dir = Path(r'D:\data_paul\Videos')
# vid_dir = Path(r'D:\data_paul\test_vids')

vids_to_track = []
for p in vid_dir.iterdir():
    if p.is_dir():
        for f in p.iterdir():
            if f.suffix == '.mpg':
                vids_to_track.append(f.as_posix())

print(vids_to_track)
dlc.analyze_videos(cfg, vids_to_track, videotype='.mpg', save_as_csv=True, robust_nframes=True,
                   allow_growth=True)