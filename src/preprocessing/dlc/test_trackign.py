import pandas as pd
from pathlib import Path
import cv2
from party import Io, cfg, tools

names = dict(
    hs_left='bodypart1',
    hs_right='bodypart2',
    body='bodypart3',
    tail='objectA'
)


workdir = r'D:\data_paul\test_vids'
vidname = '20151217'
suffix = '.mp4'
netname = 'DLC_resnet50_fig8-paulNov29shuffle1_1030000'

workdir = Path(workdir)

dlc_file = workdir / (vidname + netname + '.csv')
vid_file = workdir / (vidname + suffix)


dlc_data = pd.read_csv(dlc_file.as_posix(), index_col=0, header=[1,2])
n_frames_dlc = dlc_data.shape[0]

print(f'dlc tracked {n_frames_dlc} frames')

# Read the video
cap = cv2.VideoCapture(vid_file.as_posix())
n_frames_vid = cap.get(cv2.CAP_PROP_FRAME_COUNT)  # number frames in video
frame_width = int(cap.get(3))  # width of frame
frame_heigth = int(cap.get(4))  # heigth of frame
frate_in = cap.get(cv2.CAP_PROP_FPS)  # framerate of input video
frame_rate = int(cap.get(cv2.CAP_PROP_FPS))  # framerate of output video

print(f'video has {n_frames_vid} frames')


vidsavename = workdir / (vidname + '_tracked' + suffix)
dims = (frame_width, frame_heigth)
out = cv2.VideoWriter(vidsavename.as_posix(),
                      cv2.VideoWriter_fourcc(*'MP4V'),
                      frame_rate, dims)


current_frame = 0
pbar_print = tools.PBar(f'Printing video', 40, n_frames_vid, 3)
pbar_print.print(0)

while current_frame < n_frames_vid:
    r, f = cap.read()

    for name, real_name in names.items():
        x = int(dlc_data[real_name].loc[current_frame].x)
        y = int(dlc_data[real_name].loc[current_frame].y)

        f = cv2.circle(f, (x, y), 5, (255, 255, 0), -1)
        f = cv2.putText(f, name, (x + 20, y), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255, 0, 255))

    out.write(f)
    pbar_print.print_1()
    current_frame += 1


cap.release()
out.release()










